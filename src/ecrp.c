/* Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* mmap support */
#define _DEFAULT_SOURCE 1 /* MAP_POPULATE support */
#define _BSD_SOURCE 1 /* MAP_POPULATE for glibc < 2.19 */

#include "ecrp.h"

#include <rsys/dynamic_array_double.h>
#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/mem_allocator.h>

#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

enum { X, Y, Z, NC }; /* Helper constants */

struct ecrp {
  int64_t pagesize;
  int8_t irregular_z;
  int32_t definition[4];
  double lower[3];
  double upper[3];
  double vxsz[2]; /* Size of the voxels in X and Y */
  struct darray_double vxsz_z; /* Size of the voxels along the Z dimension */
  struct darray_double coord_z; /* Lower coordinate of the voxel along Z */

  double* Extinction; /* Mapped memory */
  double* SS_alb; /* Mapped memory */
  double* g; /* Mapped memory */
  size_t Extinction_length; /* In bytes */
  size_t SS_alb_length; /* In bytes */
  size_t g_length; /* In bytes */

  size_t pagesize_os; /* Page size of the os */
  int verbose; /* Verbosity level */
  struct logger* logger;
  struct mem_allocator* allocator;
  ref_T ref;
};

/*******************************************************************************
 * Local functions
 ******************************************************************************/
static void
log_msg
  (const struct ecrp* ecrp,
   const enum log_type stream,
   const char* msg,
   va_list vargs)
{
  ASSERT(ecrp && msg);
  if(ecrp->verbose) {
    res_T res; (void)res;
    res = logger_vprint(ecrp->logger, stream, msg, vargs);
    ASSERT(res == RES_OK);
  }
}

static void
log_err(const struct ecrp* ecrp, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(ecrp && msg);
  va_start(vargs_list, msg);
  log_msg(ecrp, LOG_ERROR, msg, vargs_list);
  va_end(vargs_list);
}

static void
reset_ecrp(struct ecrp* ecrp)
{
  ASSERT(ecrp);
  ecrp->pagesize = 0;
  ecrp->irregular_z = 0;
  ecrp->definition[0] = 0;
  ecrp->definition[1] = 0;
  ecrp->definition[2] = 0;
  ecrp->lower[0] = -1;
  ecrp->lower[1] = -1;
  ecrp->lower[2] = -1;
  ecrp->vxsz[0] = -1;
  ecrp->vxsz[1] = -1;
  darray_double_clear(&ecrp->vxsz_z);
  darray_double_clear(&ecrp->coord_z);
  if(ecrp->Extinction) munmap(ecrp->Extinction, ecrp->Extinction_length);
  if(ecrp->SS_alb) munmap(ecrp->SS_alb, ecrp->SS_alb_length);
  if(ecrp->g) munmap(ecrp->g, ecrp->g_length);
  ecrp->Extinction = NULL;
  ecrp->SS_alb = NULL;
  ecrp->g = NULL;
  ecrp->Extinction_length = 0;
  ecrp->SS_alb_length = 0;
  ecrp->g = 0;
}

static res_T
load_stream(struct ecrp* ecrp, FILE* stream, const char* stream_name)
{
  size_t nz = 0;
  size_t map_len = 0;
  size_t filesz;
  off_t offset = 0;
  res_T res = RES_OK;
  ASSERT(ecrp && stream && stream_name);

  reset_ecrp(ecrp);

  #define READ(Var, N, Name) {                                                 \
    if(fread((Var), sizeof(*(Var)), (N), stream) != (N)) {                     \
      log_err(ecrp, "%s: could not read the %s\n", stream_name, Name);         \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&ecrp->pagesize, 1, "page size");
  if(!IS_POW2(ecrp->pagesize)) {
    log_err(ecrp, "%s: invalid page size `%li'. It must be a power of two.\n",
      stream_name, (long)ecrp->pagesize);
    res = RES_BAD_ARG;
    goto error;
  }

  if(!IS_ALIGNED(ecrp->pagesize, ecrp->pagesize_os)) {
    log_err(ecrp,
"%s: invalid page size `%li'. The page size attribute must be aligned on the "
"page size of the operating system , i.e. %lu.\n",
      stream_name, (long)ecrp->pagesize,  (unsigned long)ecrp->pagesize_os);
    res = RES_BAD_ARG;
    goto error;
  }

  READ(&ecrp->irregular_z, 1, "'irregular Z' flag");
  READ(ecrp->definition, 4, "spatial definition and number of components");
  if(ecrp->definition[0] <= 0
  || ecrp->definition[1] <= 0
  || ecrp->definition[2] <= 0
  || ecrp->definition[3] <= 0) {
    log_err(ecrp,
"%s: the spatial definition or number of components cannot be negative or null -- spatial "
"definition: %i %i %i; number of components: %i\n",
      stream_name, SPLIT3(ecrp->definition), ecrp->definition[NC]);
    res = RES_BAD_ARG;
    goto error;
  }

  READ(ecrp->lower, 3, "lower position");
  READ(ecrp->vxsz, 2, "XY voxel size ");

  nz = ecrp->irregular_z ? (size_t)ecrp->definition[Z] : 1;
  res = darray_double_resize(&ecrp->vxsz_z, nz);
  if(res != RES_OK) {
    log_err(ecrp,
      "%s: could not allocate memory to store the size of the voxels in Z.\n",
      stream_name);
    goto error;
  }
  READ(darray_double_data_get(&ecrp->vxsz_z), nz, "Z voxel size(s)");
  #undef READ

  ecrp->upper[0] = ecrp->lower[0] + ecrp->vxsz[0] * ecrp->definition[0];
  ecrp->upper[1] = ecrp->lower[1] + ecrp->vxsz[1] * ecrp->definition[1];
  if(!ecrp->irregular_z) {
    ecrp->upper[2] = ecrp->lower[2]
      + darray_double_cdata_get(&ecrp->vxsz_z)[0] * ecrp->definition[2];
  } else {
    /* Compute the Z lower bound in Z of each Z slice */
    const double* size = NULL;
    double* coord = NULL;
    size_t i;
    res = darray_double_resize(&ecrp->coord_z, nz);
    if(res != RES_OK) {
      log_err(ecrp, "%s: could not allocate memory to store the lower "
        "coordinate of the voxels in Z.\n", FUNC_NAME);
      goto error;
    }
    size  = darray_double_cdata_get(&ecrp->vxsz_z);
    coord = darray_double_data_get(&ecrp->coord_z);
    FOR_EACH(i, 0, nz) coord[i] = i ? coord[i-1] + size[i-1] : ecrp->lower[2];
    ecrp->upper[2] = coord[nz-1] + size[nz-1];
  }

  map_len =
    (size_t)ecrp->definition[X]
  * (size_t)ecrp->definition[Y]
  * (size_t)ecrp->definition[Z]
  * (size_t)ecrp->definition[NC]
  * sizeof(double);
  /* Align sizeof mapped data onto page size */
  map_len = ALIGN_SIZE(map_len, (size_t)ecrp->pagesize);

  /* Ensure that offset is align on page size */
  offset = ALIGN_SIZE(ftell(stream), ecrp->pagesize);

  fseek(stream, 0, SEEK_END);
  filesz = (size_t)ftell(stream);

  #define MMAP(Var) {                                                          \
    if((size_t)offset + map_len > filesz) {                                    \
      log_err(ecrp, "%s: could not load the "STR(Var)" data.\n", stream_name); \
      log_err(ecrp, "%s: filesz %lu map_len %lu offset %lu \n", stream_name, filesz, map_len, offset); \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
    ecrp->Var = mmap(NULL, map_len, PROT_READ, MAP_PRIVATE|MAP_POPULATE,       \
      fileno(stream), offset);                                                 \
    if(ecrp->Var == MAP_FAILED) {                                              \
      log_err(ecrp, "%s: could not map the "STR(Var)" data -- %s.\n",          \
        stream_name, strerror(errno));                                         \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
    ecrp->Var##_length = map_len;                                              \
  } (void)0
  MMAP(SS_alb); offset += (off_t)map_len;
  MMAP(Extinction); offset += (off_t)map_len;
  MMAP(g); offset += (off_t)map_len;
  #undef MMAP

  CHK(fseek(stream, offset, SEEK_CUR) != -1);

exit:
  return res;
error:
  goto exit;
}

static void
release_ecrp(ref_T* ref)
{
  struct ecrp* ecrp;
  ASSERT(ref);
  ecrp = CONTAINER_OF(ref, struct ecrp, ref);
  reset_ecrp(ecrp);
  darray_double_release(&ecrp->vxsz_z);
  darray_double_release(&ecrp->coord_z);
  MEM_RM(ecrp->allocator, ecrp);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
ecrp_create
  (struct logger* in_logger,
   struct mem_allocator* mem_allocator,
   const int verbose,
   struct ecrp** out_ecrp)
{
  struct ecrp* ecrp = NULL;
  struct mem_allocator* allocator = NULL;
  struct logger* logger = NULL;
  res_T res = RES_OK;

  if(!out_ecrp) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  logger = in_logger ? in_logger : LOGGER_DEFAULT;

  ecrp = MEM_CALLOC(allocator, 1, sizeof(struct ecrp));
  if(!ecrp) {
    if(verbose) {
      logger_print(logger, LOG_ERROR,
        "%s: could not allocate the ECRP handler.\n", FUNC_NAME);
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&ecrp->ref);
  ecrp->allocator = allocator;
  ecrp->logger = logger;
  ecrp->verbose = verbose;
  ecrp->pagesize_os = (size_t)sysconf(_SC_PAGESIZE);
  darray_double_init(ecrp->allocator, &ecrp->vxsz_z);
  darray_double_init(ecrp->allocator, &ecrp->coord_z);

exit:
  if(out_ecrp) *out_ecrp = ecrp;
  return res;
error:
  if(ecrp) {
    ECRP(ref_put(ecrp));
    ecrp = NULL;
  }
  goto exit;
}

res_T
ecrp_ref_get(struct ecrp* ecrp)
{
  if(!ecrp) return RES_BAD_ARG;
  ref_get(&ecrp->ref);
  return RES_OK;
}

res_T
ecrp_ref_put(struct ecrp* ecrp)
{
  if(!ecrp) return RES_BAD_ARG;
  ref_put(&ecrp->ref, release_ecrp);
  return RES_OK;
}

res_T
ecrp_load(struct ecrp* ecrp, const char* path)
{
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!ecrp || !path) {
    res = RES_BAD_ARG;
    goto error;
  }

  file = fopen(path, "r");
  if(!file) {
    log_err(ecrp, "%s: error opening file `%s'.\n", FUNC_NAME, path);
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(ecrp, file, path);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
ecrp_load_stream(struct ecrp* ecrp, FILE* stream)
{
  if(!ecrp || !stream) return RES_BAD_ARG;
  return load_stream(ecrp, stream, "<stream>");
}

res_T
ecrp_get_desc(const struct ecrp* ecrp, struct ecrp_desc* desc)
{
  if(!ecrp || !desc) return RES_BAD_ARG;
  if(!ecrp->SS_alb) return RES_BAD_ARG;
  desc->pagesize = (size_t)ecrp->pagesize;
  desc->irregular_z = ecrp->irregular_z != 0;
  desc->spatial_definition[0] = (size_t)ecrp->definition[X];
  desc->spatial_definition[1] = (size_t)ecrp->definition[Y];
  desc->spatial_definition[2] = (size_t)ecrp->definition[Z];
  desc->ncomponents = (size_t)ecrp->definition[NC];
  desc->lower[0] = ecrp->lower[0];
  desc->lower[1] = ecrp->lower[1];
  desc->lower[2] = ecrp->lower[2];
  desc->upper[0] = ecrp->upper[0];
  desc->upper[1] = ecrp->upper[1];
  desc->upper[2] = ecrp->upper[2];
  desc->vxsz_x = ecrp->vxsz[0];
  desc->vxsz_y = ecrp->vxsz[1];
  desc->vxsz_z = darray_double_cdata_get(&ecrp->vxsz_z);
  desc->coord_z = ecrp->irregular_z ? darray_double_cdata_get(&ecrp->coord_z) : NULL;
  desc->Extinction = ecrp->Extinction;
  desc->SS_alb = ecrp->SS_alb;
  desc->g = ecrp->g;
  return RES_OK;
}

