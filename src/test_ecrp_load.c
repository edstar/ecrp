/* Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "ecrp.h"
#include "test_ecrp_utils.h"

#include <rsys/double3.h>

#include <unistd.h>

int
main(int argc, char** argv)
{
  int64_t pagesize;
  struct ecrp* ecrp = NULL;
  struct ecrp_desc desc = ECRP_DESC_NULL;
  FILE* stream = NULL;
  int8_t i8;
  int32_t i32[4];
  double dbl[3];
  size_t i, n;
  size_t x, y, z, ic;
  (void)argc, (void)argv;

  stream = tmpfile();

  pagesize = (int64_t)sysconf(_SC_PAGESIZE); /* pagesize */
  CHK(fwrite(&pagesize, sizeof(pagesize), 1, stream) == 1);
  i8 = 0; /* irregular Z */
  CHK(fwrite(&i8, sizeof(i8), 1, stream) == 1);
  i32[0] = 2; /* Definition */
  i32[1] = 2;
  i32[2] = 3;
  i32[3] = 1;
  CHK(fwrite(i32, sizeof(i32[0]), 4, stream) == 4);
  dbl[0] = 0; /* Lower position */
  dbl[1] = 0;
  dbl[2] = 0;
  CHK(fwrite(dbl, sizeof(dbl[0]), 3, stream) == 3);
  dbl[0] = 1; /* Voxel size */
  dbl[1] = 2;
  dbl[2] = 3;
  CHK(fwrite(dbl, sizeof(dbl[0]), 3, stream) == 3);

  n = (size_t)(i32[0]*i32[1]*i32[2]*i32[3]);
  fseek(stream, ALIGN_SIZE(ftell(stream), pagesize), SEEK_SET); /* padding */
  FOR_EACH(i, 0, n) { /* SS_alb */
    dbl[0] = (double)i;
    CHK(fwrite(dbl, sizeof(dbl[0]), 1, stream) == 1);
  }

  fseek(stream, ALIGN_SIZE(ftell(stream), pagesize), SEEK_SET); /* padding */
  FOR_EACH(i, 0, n) { /* Extinction */
    dbl[0] =-(double)i;
    CHK(fwrite(dbl, sizeof(dbl[0]), 1, stream) == 1);
  }
  fseek(stream, ALIGN_SIZE(ftell(stream), pagesize), SEEK_SET); /* padding */

  FOR_EACH(i, 0, n) { /* g */
    dbl[0] =(double)(i*2);
    CHK(fwrite(dbl, sizeof(dbl[0]), 1, stream) == 1);
  }

  if(!IS_ALIGNED(ftell(stream), (size_t)pagesize)) {
    /* Padding to ensure that the size is aligned on the page size. Note that
     * one char is written to positioned the EOF indicator */
    const char byte = 0;
    fseek(stream, ALIGN_SIZE(ftell(stream), pagesize)-1, SEEK_SET);
    CHK(fwrite(&byte, 1, 1, stream) == 1);
  }

  rewind(stream);

  CHK(ecrp_create(NULL, &mem_default_allocator, 1, &ecrp) == RES_OK);

  CHK(ecrp_get_desc(NULL, &desc) == RES_BAD_ARG);
  CHK(ecrp_get_desc(ecrp, NULL) == RES_BAD_ARG);
  CHK(ecrp_get_desc(ecrp, &desc) == RES_BAD_ARG);

  CHK(ecrp_load_stream(NULL, stream) == RES_BAD_ARG);
  CHK(ecrp_load_stream(ecrp, NULL) == RES_BAD_ARG);
  CHK(ecrp_load_stream(ecrp, stream) == RES_OK);

  fclose(stream);

  CHK(ecrp_get_desc(NULL, &desc) == RES_BAD_ARG);
  CHK(ecrp_get_desc(ecrp, NULL) == RES_BAD_ARG);
  CHK(ecrp_get_desc(ecrp, &desc) == RES_OK);

  CHK(desc.pagesize == 4096);
  CHK(desc.irregular_z == 0);
  CHK(desc.spatial_definition[0] == 2);
  CHK(desc.spatial_definition[1] == 2);
  CHK(desc.spatial_definition[2] == 3);
  CHK(desc.ncomponents == 1);
  CHK(desc.lower[0] == 0);
  CHK(desc.lower[1] == 0);
  CHK(desc.lower[2] == 0);
  CHK(desc.upper[0] == desc.vxsz_x * (double)desc.spatial_definition[0]);
  CHK(desc.upper[1] == desc.vxsz_y * (double)desc.spatial_definition[1]);
  CHK(desc.upper[2] == desc.vxsz_z[0] * (double)desc.spatial_definition[2]);
  CHK(desc.vxsz_x == 1);
  CHK(desc.vxsz_y == 2);
  CHK(desc.vxsz_z[0] == 3);
  CHK(desc.coord_z == NULL);

  FOR_EACH(x, 0, desc.spatial_definition[0]) {
    double low[3];
    low[0] = (double)x * desc.vxsz_x;
    FOR_EACH(y, 0, desc.spatial_definition[1]) {
      low[1] = (double)y * desc.vxsz_y;
      FOR_EACH(z, 0, desc.spatial_definition[2]) {
        double vox_low[3], vox_upp[3];
        double upp[3];
        low[2] = (double)z * desc.vxsz_z[0];
        upp[0] = low[0] + desc.vxsz_x;
        upp[1] = low[1] + desc.vxsz_y;
        upp[2] = low[2] + desc.vxsz_z[0];
        ecrp_desc_get_voxel_aabb(&desc, x, y, z, vox_low, vox_upp);
        CHK(d3_eq_eps(vox_low, low, 1.e-6));
        CHK(d3_eq_eps(vox_upp, upp, 1.e-6));
      }
    }
  }

  n = desc.spatial_definition[0]
    * desc.spatial_definition[1]
    * desc.spatial_definition[2]
    * desc.ncomponents;
  FOR_EACH(i, 0,n) {
    CHK(desc.SS_alb[i] == (double)i);
    CHK(desc.Extinction[i] ==-(double)i);
  }
  i = 0;
  FOR_EACH(ic, 0, desc.ncomponents) {
  FOR_EACH(z, 0, desc.spatial_definition[2]) {
  FOR_EACH(y, 0, desc.spatial_definition[1]) {
  FOR_EACH(x, 0, desc.spatial_definition[0]) {
    CHK(ecrp_desc_SS_alb_at(&desc, x, y, z, ic) == (double)i);
    CHK(ecrp_desc_Extinction_at(&desc, x, y, z, ic) ==-(double)i);
    CHK(ecrp_desc_g_at(&desc, x, y, z, ic) ==(double)(i*2));
    ++i;
  }}}}
  CHK(ecrp_ref_put(ecrp) == RES_OK);

  CHK(mem_allocated_size() == 0);
  return 0;
}
