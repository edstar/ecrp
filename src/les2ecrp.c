/* Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* getopt/close support */

#include "les2ecrp.h"

#include <rsys/cstr.h>
#include <rsys/math.h>
#include <rsys/mem_allocator.h>
#include <rsys/rsys.h>

#include <alloca.h>
#include <errno.h>
#include <netcdf.h>
#include <string.h>
#include <unistd.h> /* getopt */
#include <fcntl.h> /* open */
#include <sys/stat.h> /* S_IRUSR & S_IWUSR */

/*******************************************************************************
 * Command arguments
 ******************************************************************************/
struct args {
  const char* output;
  const char* input;
  double fp_to_meter;
  long pagesize;
  int force_overwrite;
  int check;
  int no_output;
  int quit; /* Quit the application */
};
#define ARGS_DEFAULT__ {NULL,NULL,1.0,4096,0,0,0,0}
static const struct args ARGS_DEFAULT = ARGS_DEFAULT__;

static void
print_help(const char* cmd)
{
  ASSERT(cmd);
  (void)cmd;

  printf(
"Usage: les2ecrp -i INPUT [OPTIONS]\n"
"Convert the LES data stored into INPUT from NetCDF to the ecrp fileformat.\n\n");
  printf(
"  -c               advanced check of the validity of the submitted LES file.\n");
  printf(
"  -f               overwrite the OUTPUT file if it already exists.\n");
  printf(
"  -h               display this help and exit.\n");
  printf(
"  -i INPUT         path of the LES file to convert.\n");
  printf(
"  -m FLT_TO_METER  scale factor to convert from floating point units to\n"
"                   meters. By default, it is set to %g.\n",
    ARGS_DEFAULT.fp_to_meter);
  printf(
"  -o OUTPUT        write results to OUTPUT. If not defined, write results to\n"
"                   standard output.\n");
  printf(
"  -p               targeted page size in bytes. Must be a power of 2. By\n"
"                   default, the page size is %li Bytes.\n",
    ARGS_DEFAULT.pagesize);
  printf(
"  -q               do not write results to OUTPUT.\n");
  printf(
"  -v               display version information and exit.\n");
  printf("\n");
  printf(
"Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com).\n"
"Copyright (C) 2018 CNRS, Université Paul Sabatier. les2ecrp is free software\n"
"released under the GNU GPL license, version 3 or later. You are free to\n"
"change or redistribute it under certain conditions\n"
"<http://gnu.org/licenses/gpl.html>\n");
}

static void
args_release(struct args* args)
{
  ASSERT(args);
  *args = ARGS_DEFAULT;
}

static res_T
args_init(struct args* args, const int argc, char** argv)
{
  int opt;
  res_T res = RES_OK;
  ASSERT(args && argc && argv);

  while((opt = getopt(argc, argv, "cfhi:m:o:p:qv")) != -1) {
    switch(opt) {
      case 'c': args->check = 1; break;
      case 'f': args->force_overwrite = 1; break;
      case 'h':
        print_help(argv[0]);
        args_release(args);
        args->quit = 1;
        goto exit;
      case 'i': args->input = optarg; break;
      case 'm':
        res = cstr_to_double(optarg, &args->fp_to_meter);
        if(res == RES_OK && args->fp_to_meter <= 0) res = RES_BAD_ARG;
        break;
      case 'o': args->output = optarg; break;
      case 'p':
        res = cstr_to_long(optarg, &args->pagesize);
        if(res == RES_OK && !IS_POW2(args->pagesize)) res = RES_BAD_ARG;
        break;
      case 'q': args->no_output = 1; break;
      case 'v':
        printf("%s %d.%d.%d\n",
          argv[0],
          LES2HTLES_VERSION_MAJOR,
          LES2HTLES_VERSION_MINOR,
          LES2HTLES_VERSION_PATCH);
        args->quit = 1;
        goto exit;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }

  if(!args->input) {
    fprintf(stderr, "Missing input file.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  if(args->no_output) args->output = NULL;

exit:
  return res;
error:
  args_release(args);
  goto exit;
}

/*******************************************************************************
 * Grid header
 ******************************************************************************/
struct grid {
  int32_t nx, ny, nz, ncomponents; /* Definition */
  int8_t is_z_irregular;
  double lower[3]; /* Lower position of the grid */
  double vxsz_x; /* Size of the voxel is X */
  double vxsz_y; /* Size of the voxel in Y */
  double* vxsz_z; /* Size of the voxel in Z */
};

#define GRID_NULL__ {0,0,0,0,0,{0,0,0},0,0,NULL}
static const struct grid GRID_NULL = GRID_NULL__;

static void
grid_release(struct grid* grid)
{
  CHK(grid);
  if(grid->vxsz_z) mem_rm(grid->vxsz_z);
}

/*******************************************************************************
 * NetCDF helper functions and macros
 ******************************************************************************/
#define INVALID_ID -1

#define NC(Func) {                                                             \
    const int err__ = nc_ ## Func;                                             \
    if(err__ != NC_NOERR) {                                                    \
      fprintf(stderr, "error:%i:%s\n", __LINE__, ncerr_to_str(err__));         \
      abort();                                                                 \
    }                                                                          \
  } (void)0

#define NDIMS_MAX 4

static INLINE const char*
ncerr_to_str(const int err)
{
  const char* str = "NC_ERR_<UNKNOWN>";
  switch(err) {
    case NC_EBADGRPID: str = "NC_EBADGRPID"; break;
    case NC_EBADID: str = "NC_EBADID"; break;
    case NC_EBADNAME: str = "NC_EBADNAME"; break;
    case NC_ECHAR: str = "NC_ECHAR"; break;
    case NC_EDIMMETA: str = "NC_EDIMMETA"; break;
    case NC_EHDFERR: str = "NC_EHDFERR"; break;
    case NC_EINVAL: str = "NC_EINVAL"; break;
    case NC_ENOMEM: str = "NC_ENOMEM"; break;
    case NC_ENOTATT: str = "NC_ENOTATT"; break;
    case NC_ENOTVAR: str = "NC_ENOTVAR"; break;
    case NC_ERANGE: str = "NC_ERANGE"; break;
    case NC_NOERR: str = "NC_NOERR"; break;
  }
  return str;
}

static INLINE const char*
nctype_to_str(const nc_type type)
{
  const char* str = "NC_TYPE_<UNKNOWN>";
  switch(type) {
    case NC_NAT: str = "NC_NAT"; break;
    case NC_BYTE: str = "NC_BYTE"; break;
    case NC_CHAR: str = "NC_CHAR"; break;
    case NC_SHORT: str = "NC_SHORT"; break;
    case NC_LONG: str = "NC_LONG"; break;
    case NC_FLOAT: str = "NC_FLOAT"; break;
    case NC_DOUBLE: str = "NC_DOUBLE"; break;
    case NC_UBYTE: str = "NC_UBYTE"; break;
    case NC_USHORT: str = "NC_USHORT"; break;
    case NC_UINT: str = "NC_UINT"; break;
    case NC_INT64: str = "NC_INT64"; break;
    case NC_UINT64: str = "NC_UINT64"; break;
    case NC_STRING: str = "NC_STRING"; break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return str;
}

static INLINE size_t
sizeof_nctype(const nc_type type)
{
  size_t sz;
  switch(type) {
    case NC_BYTE:
    case NC_CHAR:
    case NC_UBYTE:
      sz = 1; break;
    case NC_SHORT:
    case NC_USHORT:
      sz = 2; break;
    case NC_FLOAT:
    case NC_INT:
    case NC_UINT:
      sz = 4; break;
    case NC_DOUBLE:
    case NC_INT64:
    case NC_UINT64:
      sz = 8; break;
    default: FATAL("Unreachable cde.\n"); break;
  }
  return sz;
}

struct var {
  size_t dim_len[NDIMS_MAX]; /* Length of the dimensions */
  int ndims; /* #dimensions */
  int id; /* NetCDF identifier */
  int type; /* Variable type */
};
static const struct var VAR_NULL = {{0}, 0, 0, 0};

static INLINE res_T
ncvar_real_inq
  (int nc,
   const char* var_name,
   const size_t expected_ndims,
   struct var* var)
{
  int i;
  int dimids[NDIMS_MAX]; /* NetCDF id of the data dimension */
  int err;
  res_T res = RES_OK;
  ASSERT(var_name && var && expected_ndims && expected_ndims <= NDIMS_MAX);

  /* Retrieve the NetCDF id of the variable */
  err = nc_inq_varid(nc, var_name, &var->id);
  if(err != NC_NOERR) {
    fprintf(stderr, "Could not inquire the '%s' variable -- %s\n",
      var_name, ncerr_to_str(err));
    res = RES_BAD_ARG;
    goto error;
  }

  /* Inquire the dimensions of the variable */
  NC(inq_varndims(nc, var->id, &var->ndims));
  if((size_t)var->ndims != expected_ndims) {
    fprintf(stderr, "The dimension of the '%s' variable must be %lu .\n",
      var_name, (unsigned long)(expected_ndims));
    res = RES_BAD_ARG;
    goto error;
  }
  NC(inq_vardimid(nc, var->id, dimids));
  FOR_EACH(i, 0, var->ndims) NC(inq_dimlen(nc, dimids[i], var->dim_len+i));

  /* Inquire the type of the variable */
  NC(inq_vartype(nc, var->id, &var->type));
  if(var->type != NC_DOUBLE && var->type != NC_FLOAT) {
    fprintf(stderr,
      "The type of the '%s' variable cannot be %s. Expecting floating point data.\n",
      var_name, nctype_to_str(var->type));
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
open_output_stream(const char* path, const int force_overwrite, FILE** stream)
{
  int fd = -1;
  FILE* fp = NULL;
  res_T res = RES_OK;
  ASSERT(path && stream);

  if(force_overwrite) {
    fp = fopen(path, "w");
    if(!fp) {
      fprintf(stderr, "Could not open the output file '%s'.\n", path);
      goto error;
    }
  } else {
    fd = open(path, O_CREAT|O_WRONLY|O_EXCL|O_TRUNC, S_IRUSR|S_IWUSR);
    if(fd >= 0) {
      fp = fdopen(fd, "w");
      if(fp == NULL) {
        fprintf(stderr, "Could not open the output file '%s'.\n", path);
        goto error;
      }
    } else if(errno == EEXIST) {
      fprintf(stderr,
        "The output file '%s' already exists. Use -f to overwrite it.\n", path);
      goto error;
    } else {
      fprintf(stderr,
        "Unexpected error while opening the output file `'%s'.\n", path);
      goto error;
    }
  }

exit:
  *stream = fp;
  return res;
error:
  res = RES_IO_ERR;
  if(fp) {
    CHK(fclose(fp) == 0);
    fp = NULL;
  } else if(fd >= 0) {
    CHK(close(fd) == 0);
  }
  goto exit;
}


static res_T
setup_definition
  (int nc, int32_t* nx, int32_t* ny, int32_t* nz, int32_t* ncomponents)
{
  size_t len[4];
  int dimids[4];
  int id;
  int ndims;
  int err = NC_NOERR;
  res_T res = RES_OK;
  ASSERT(nx && ny && nz && ncomponents);

  err = nc_inq_varid(nc, "Extinction", &id);
  if(err != NC_NOERR) {
    fprintf(stderr, "Could not inquire the 'Extinction' variable -- %s\n",
      ncerr_to_str(err));
    res = RES_BAD_ARG;
    goto error;
  }

  NC(inq_varndims(nc, id, &ndims));
  if(ndims != 4) {
    fprintf(stderr, "The dimension of the 'Extinction' variable must be 4.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  NC(inq_vardimid(nc, id, dimids));
  NC(inq_dimlen(nc, dimids[0], len+0));
  NC(inq_dimlen(nc, dimids[1], len+1));
  NC(inq_dimlen(nc, dimids[2], len+2));
  NC(inq_dimlen(nc, dimids[3], len+3));

  *nx = (int32_t)len[3];
  *ny = (int32_t)len[2];
  *nz = (int32_t)len[1];
  *ncomponents = (int32_t)len[0];

  if(*nx < 1 || *ny < 1 || *nz < 1 || *ncomponents < 1) {
    fprintf(stderr,
      "The spatial definition and number of components cannot be null.\n"
      "  #x = %i; #y = %i; #z = %i; #components = %i\n",
      *nx, *ny, *nz, *ncomponents);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
setup_regular_dimension
  (const int nc,
   const char* var_name,
   double* voxel_size,
   double* lower_pos,
   const int check)
{
  struct var var = VAR_NULL;
  double* mem = NULL;
  size_t start, i;
  double vxsz;
  res_T res = RES_OK;
  ASSERT(nc && var_name && voxel_size && lower_pos);

  res = ncvar_real_inq(nc, var_name, 1, &var);
  if(res != RES_OK) goto error;

  if(!check) {
    const size_t len = 1;
    start = 0;
    NC(get_vara_double(nc, var.id, &start, &len, &vxsz));
    /* Assume that the coordinate is defined at the center of the cell and that
     * the grid starts at 0 */
    vxsz *= 2;
  } else {
    mem = mem_alloc(var.dim_len[0]*sizeof(double));
    if(!mem) {
      fprintf(stderr, "Could not allocate memory for the '%s' variable.\n", var_name);
      res = RES_MEM_ERR;
      goto error;
    }
    start = 0;
    NC(get_vara_double(nc, var.id, &start, &var.dim_len[0], mem));
    /* Assume that the coordinate is defined at the center of the cell and that
     * the grid starts at 0 */
    vxsz = mem[0] * 2;
  }

  if(vxsz <= 0) {
    fprintf(stderr, "The '%s' variable can't have negative or null values.\n",
      var_name);
    res = RES_BAD_ARG;
    goto error;
  }

  if(check) {
    /* Check that the dimension is regular */
    FOR_EACH(i, 1, var.dim_len[0]) {
      if(!eq_eps(mem[i] - mem[i-1], vxsz, 1.e-6)) {
        fprintf(stderr, "The voxel size of the '%s' variable must be regular.\n",
          var_name);
        res = RES_BAD_ARG;
        goto error;
      }
    }
  }

  *voxel_size = vxsz;
  *lower_pos = 0;
exit:
  if(mem) mem_rm(mem);
  return res;
error:
  goto exit;
}

static res_T
compute_Z_voxel_size
  (const char* var_name,
   const double* lvls,
   const size_t nlvls,
   double**  voxel_size,
   double* lower_pos,
   int8_t* is_z_irregular)
{
  double vxsz; /* Voxel size */
  double* pvxsz = NULL; /* Pointer toward voxel sizes */
  size_t z;
  int irregular;
  res_T res = RES_OK;
  ASSERT(var_name && lvls && nlvls && voxel_size && lower_pos && is_z_irregular);

  /* Assume that the coordinate is defined at the center of the cell and that
   * the grid starts at 0 */
  vxsz = lvls[0] * 2.0;
  if(vxsz <= 0) {
    fprintf(stderr,
      "The '%s' variable can't have negative or null values.\n", var_name);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Check if the Z dimension is regular */
  FOR_EACH(z, 1, nlvls) {
    if(!eq_eps(lvls[z] - lvls[z-1], vxsz, 1.e-6)) break;
  }
  irregular = (z != nlvls);

  pvxsz = mem_alloc(sizeof(double) * (irregular ? nlvls : 1));
  if(!pvxsz) {
    fprintf(stderr,
      "Could not allocate memory for the voxel size along the Z dimension.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  pvxsz[0] = vxsz;

  if(irregular) {
    FOR_EACH(z, 1, nlvls) {
      pvxsz[z] = (lvls[z] - (lvls[z-1] + pvxsz[z-1]*0.5)) * 2.0;
      if(pvxsz[z] <= 0) {
        fprintf(stderr,
          "The '%s' variable can't have negative or null values.\n", var_name);
        res = RES_BAD_ARG;
        goto error;
      }
    }
  }

  *lower_pos = 0;
  *voxel_size = pvxsz;
  *is_z_irregular = (int8_t)irregular;

exit:
  return res;
error:
  if(pvxsz) mem_rm(pvxsz);
  goto exit;
}

static res_T
setup_Z_dimension_height
  (const int nc,
   double** voxel_size,
   double* lower_pos,
   int8_t* is_z_irregular,
   const int check)
{
  struct var var = VAR_NULL;
  double* mem = NULL;
  size_t start = 0;
  res_T res = RES_OK;
  (void)check;
  ASSERT(nc && voxel_size && lower_pos);

  res = ncvar_real_inq(nc, "height", 1, &var);
  if(res != RES_OK) goto error;

  mem = mem_alloc(var.dim_len[0]*sizeof(double));
  if(!mem) {
    fprintf(stderr,
      "Could not allocate memory for the 'height' variable.\n");
    res = RES_MEM_ERR;
    goto error;
  }

  NC(get_vara_double(nc, var.id, &start, &var.dim_len[0], mem));

  res = compute_Z_voxel_size("height", mem, var.dim_len[0],
    voxel_size, lower_pos, is_z_irregular);
  if(res != RES_OK) goto error;

exit:
  if(mem) mem_rm(mem);
  return res;
error:
  goto exit;
}

static res_T
setup_Z_dimension
  (const int nc,
   double** voxel_size,
   double*  lower_pos,
   int8_t* is_z_irregular,
   const int check)
{
  int err;
  int id;
  res_T res = RES_OK;
  ASSERT(nc);

  err = nc_inq_varid(nc, "height", &id);
  if(err != NC_NOERR) {
      fprintf(stderr,
        "Could not inquire the 'height' "
        "variable -- %s\n", ncerr_to_str(err));
      res = RES_BAD_ARG;
      goto error;
    }
  res = setup_Z_dimension_height
      (nc, voxel_size, lower_pos, is_z_irregular, check);
  if(res != RES_OK) goto error;
exit:
  return res;
error:
  goto exit;
}

/* Functor used to convert NetCDF data */
typedef void (*convert_data_T)
  (double* data, /* Input data */
   const size_t start[3], /* Start Index of the data in the NetCDF */
   const size_t len[3], /* Lenght of the loaded data */
   void* context); /* User defined data */

/* Write data of the variable "var_name" into "stream". The data are loaded
 * slice by slice, where each slice is a 2D array whose dimensions are
 * "x_scene X y_scene". The "convert" functor is invoked on each loaded slice.
 */
static res_T
write_data
  (int nc,
   const char* var_name,
   FILE* stream,
   convert_data_T convert_func, /* May be NULL <=> No conversion */
   void* convert_ctx) /* Sent as the last argument of "convert_func" */
{
  enum { NCOMPONENTS, Z, Y, X, NDIMS }; /* Helper constants */

  struct var var = VAR_NULL;
  double* mem = NULL; /* Memory where NetCDF data are copied */
  size_t start[NDIMS];
  size_t len[NDIMS];
  size_t slice_len; /* #elements per slice the 3D grid */
  size_t ic; /* Id over the NCOMPONENTS dimension */
  size_t z; /* Id over the Z dimension */
  res_T res = RES_OK;
  CHK(var_name);

  res = ncvar_real_inq(nc, var_name, NDIMS, &var);
  if(res != RES_OK) goto error;

  /* Alloc the memory space where the variable data will be copied. Note that,
   * in order to reduce memory consumption, only one slice is read at a given
   * Z position. */
  slice_len = var.dim_len[X]*var.dim_len[Y]; /* # elements per slice */
  mem = mem_alloc(slice_len*sizeof(double));

  if(!mem) {
    fprintf(stderr, "Could not allocate memory for the '%s' variable.\n",
      var_name);
    res = RES_MEM_ERR;
    goto error;
  }

  /* Read the whole slice */
  start[X] = 0; len[X] = var.dim_len[X];
  start[Y] = 0; len[Y] = var.dim_len[Y];

  FOR_EACH(ic, 0, var.dim_len[NCOMPONENTS]) {
    start[NCOMPONENTS] = ic; len[NCOMPONENTS] = 1;
    FOR_EACH(z, 0, var.dim_len[Z]) {
      size_t n;
      start[Z] = z; len[Z] = 1;

      NC(get_vara_double(nc, var.id, start, len, mem)); /* Fetch the slice data */

      if(convert_func) convert_func(mem, start, len, convert_ctx);

      /* Write data */
      n = fwrite(mem, sizeof(double), slice_len, stream);
      if(n != slice_len) {
        fprintf(stderr, "Error writing data of the '%s' variable -- '%s'.\n",
          var_name, strerror(errno));
        res = RES_IO_ERR;
        goto error;
      }
    }
  }

exit:
  if(mem) mem_rm(mem);
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Program
 ******************************************************************************/
int
main(int argc, char** argv)
{
  FILE* stream = stdout;
  struct grid grid = GRID_NULL;
  struct args args = ARGS_DEFAULT;

  int err = 0;
  int err_nc = NC_NOERR;
  int nc = INVALID_ID;
  res_T res = RES_OK;

  res = args_init(&args, argc, argv);
  if(res != RES_OK) goto error;
  if(args.quit) goto exit;

  if(args.output) {
    res = open_output_stream(args.output, args.force_overwrite, &stream);
    if(res != RES_OK) goto error;
  }

  err_nc = nc_open(args.input, NC_NOWRITE, &nc);
  if(err_nc != NC_NOERR) {
    fprintf(stderr, "Error opening file `%s' -- %s.\n",
      args.input, ncerr_to_str(err_nc));
    goto exit;
  }

  #define CALL(Func) { if(RES_OK != (res = Func)) goto error; } (void)0

  CALL(setup_definition(nc, &grid.nx, &grid.ny, &grid.nz, &grid.ncomponents));
  CALL(setup_regular_dimension
    (nc, "x_scene", &grid.vxsz_x, grid.lower+0, args.check));
  CALL(setup_regular_dimension
    (nc, "y_scene", &grid.vxsz_y, grid.lower+1, args.check));
  CALL(setup_Z_dimension
    (nc, &grid.vxsz_z, grid.lower+2, &grid.is_z_irregular, args.check));

  if(args.fp_to_meter != 1) { /* Convert the grid in meters */
    grid.lower[0] *= args.fp_to_meter;
    grid.lower[1] *= args.fp_to_meter;
    grid.lower[2] *= args.fp_to_meter;

    grid.vxsz_x *= args.fp_to_meter;
    grid.vxsz_y *= args.fp_to_meter;
    if(!grid.is_z_irregular) {
      grid.vxsz_z[0] *= args.fp_to_meter;
    } else {
      int32_t i;
      FOR_EACH(i, 0, grid.nz) {
        grid.vxsz_z[i] *= args.fp_to_meter;
      }
    }
  }

  #define WRITE(Var, N, Name) {                                                \
    if(fwrite((Var), sizeof(*(Var)), (N), stream) != (N)) {                    \
      fprintf(stderr, "Error writing the %s.\n", (Name));                      \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  if(!args.no_output) {
    const int64_t pagesz = (int64_t)args.pagesize;

    WRITE(&pagesz, 1, "page size");
    WRITE(&grid.is_z_irregular, 1, "'irregular' Z boolean");
    WRITE(&grid.nx, 1, "X definition");
    WRITE(&grid.ny, 1, "Y definition");
    WRITE(&grid.nz, 1, "Z definition");
    WRITE(&grid.ncomponents, 1, "number of components");
    WRITE(grid.lower, 3, "lower position");
    WRITE(&grid.vxsz_x, 1, "X voxel size");
    WRITE(&grid.vxsz_y, 1, "Y voxel size");
    WRITE(grid.vxsz_z, grid.is_z_irregular?(size_t)grid.nz:1, "Z voxel size(s)");

    fseek(stream, ALIGN_SIZE(ftell(stream),(off_t)pagesz), SEEK_SET);/*padding*/
    CALL(write_data(nc, "SS_alb", stream, NULL, NULL));

    fseek(stream, ALIGN_SIZE(ftell(stream),(off_t)pagesz), SEEK_SET);/*padding*/
    CALL(write_data(nc, "Extinction", stream, NULL, NULL));

    fseek(stream, ALIGN_SIZE(ftell(stream),(off_t)pagesz), SEEK_SET);/*padding*/
    CALL(write_data(nc, "g", stream, NULL, NULL));

    if(!IS_ALIGNED(ftell(stream), (size_t)pagesz)) {
      /* Padding to ensure that the size is aligned on the page size. Note that
       * one char is written to position the EOF indicator */
      const char byte = 0;
      fseek(stream, ALIGN_SIZE(ftell(stream),(off_t)pagesz)-1, SEEK_SET);
      WRITE(&byte, 1, "Dummy Byte");
    }
  }
  #undef WRITE
  #undef CALL

exit:
  grid_release(&grid);
  if(stream && stream != stdout) CHK(fclose(stream) == 0);
  if(nc != INVALID_ID) NC(close(nc));
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Memory leaks: %lu Bytes\n",
      (unsigned long)mem_allocated_size());
    err = -1;
  }
  return err;
error:
  err = -1;
  goto exit;
}

